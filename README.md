# colorapi

To run the app

1. install requirements:
`pip install -r requirements.txt`

2. cd into the project folder
`cd color-picker`

3. run server
`python manage.py runserver`

4. Go to:
`http://127.0.0.1:8000/api/?src=URL_OF_THE_IMAGE`


Currently shows only the dominant color.
