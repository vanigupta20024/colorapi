from django.http import HttpResponse
from colorthief import ColorThief
from urllib.request import urlopen
import json
import io
import os
import requests

def get_dominant(request):
    url = str(request.GET['src'])
    response = requests.get(url, stream = True)
    with open('out.jpg', 'wb') as f:
        f.write(response.content)
    color_thief = ColorThief('out.jpg')
    dominant_color = color_thief.get_color(quality=1)
    data = {
        'dominant color':('#%02x%02x%02x' % dominant_color)
    }
    return HttpResponse(json.dumps(data, indent=4))